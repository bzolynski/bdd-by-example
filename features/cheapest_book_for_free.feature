Feature: Cheapest book for free when 5 books purchased
  In order to get the cheapest book for free
  As a customer
  I need to put at least 5 books into the basket

  Scenario: Buying 5 books
    Given I have 4 books in the basket
    When I add a cheaper book to the basket
    Then the cheapest book should be for free
