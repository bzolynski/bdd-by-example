Feature: Basket with different delivery cost
  In order to buy books
  As a customer
  I need to be able to put interesting books into a basket

  Rules:
  - VAT is 20% (included in all prices for a customer)
  - Delivery for basket under EUR 20 is EUR 3
  - Delivery for basket over EUR 20 is EUR 2

  Scenario: Buying a single book under EUR 20
    Given there is a "War and Peace" book, which costs EUR 17
    When I add the "War and Peace" book to the basket
    Then the overall basket price should be EUR 20

  Scenario: Buying a single book over EUR 20
    Given there is a "Catch 22" book, which costs EUR 22
    When I add the "Catch 22" book to the basket
    Then the overall basket price should be EUR 24

  Scenario: Buying two books over EUR 20
    Given there is a "War and Peace" book, which costs EUR 17
    And there is a "Catch 22" book, which costs EUR 22
    When I add the "War and Peace" book to the basket
    And I add the "Catch 22" book to the basket
    Then the overall basket price should be EUR 41
