<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use CODEfactors\BddByExample\Book;
use CODEfactors\BddByExample\Basket;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    private $books;

    private $basket;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->basket = new Basket;
    }

    /**
     * @Given there is a :title book, which costs EUR :price
     */
    public function thereIsAWhichCostsEur($title, $price)
    {
        $this->books[$title] = new Book($title, $price);
    }

    /**
     * @When I add the :title book to the basket
     */
    public function iAddTheToTheBasket($title)
    {
        if (!isset($this->books[$title])) {
            throw new Exception('Book not defined');
        }
        $this->basket->addBook($this->books[$title]);
    }

    /**
     * @Then the overall basket price should be EUR :totalPrice
     */
    public function theOverallBasketPriceShouldBeEur($totalPrice)
    {
        Assert::assertEquals($totalPrice, $this->basket->getTotalPrice());
    }

    /**
     * @Given I have :arg1 books in the basket
     */
    public function iHaveBooksInTheBasket($booksAmount)
    {
        for ($i = 1; $i <= $booksAmount; $i++) {
            $this->basket->addBook(new Book('Title of book', 15));
        }
    }

    /**
     * @When I add a cheaper book to the basket
     */
    public function iAddTheCheaperBookToTheBasket()
    {
        $this->basket->addBook(new Book('Title of book', 13));
    }

    /**
     * @Then the cheapest book should be for free
     */
    public function theCheapestBookShouldBeForFree()
    {
        $totalPrice = 15 * 4 + 2.0;
        Assert::assertEquals($totalPrice, $this->basket->getTotalPrice());
    }
}
