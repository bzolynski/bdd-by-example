all : composer
.PHONY: composer

all : composer
.PHONY: all start stop composer phpunit

start :
	docker-compose up -d

stop :
	docker-compose down

composer :
	docker-compose run --rm --no-deps php composer install

composer-require :
	docker-compose run --rm --no-deps php composer require $(package)

composer-require-dev :
	docker-compose run --rm --no-deps php composer require --dev $(package)

composer-remove-dev :
	docker-compose run --rm --no-deps php composer remove --dev $(package)

phpunit :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests

phpunit-coverage-text :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-text

phpunit-coverage-html :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-html coverage/

behat-init :
	docker-compose run --rm --no-deps php vendor/bin/behat --init

behat :
	docker-compose run --rm --no-deps php vendor/bin/behat

behat-snippets :
	docker-compose run --rm --no-deps php vendor/bin/behat --dry-run --append-snippets
