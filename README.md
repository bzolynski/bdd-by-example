
BDD by example
==============

BDD example with a spice of TDD.

1. On the beginning, we're writing the rules for features.

        Feature: Basket with different delivery cost
          In order to buy books
          As a customer
          I need to be able to put interesting books into a basket
        
          Rules:
          - VAT is 20%
          - Delivery for basket under EUR 20 is EUR 3
          - Delivery for basket over EUR 20 is EUR 2
        
          Scenario: Buying a single book under EUR 20
            Given there is a "War and Peace", which costs EUR 17
            When I add the "War and Peace" to the basket
            Then the overall basket price should be EUR ??
        
          Scenario: Buying a single book over EUR 20
            Given there is a "Catch 22", which costs EUR 22
            When I add the "Catch 22" to the basket
            Then the overall basket price should be EUR ??
        
          Scenario: Buying two books over EUR 20
            Given there is a "War and Peace", which costs EUR 17
            And there is a "Catch 22", which costs EUR 22
            When I add the "War and Peace" to the basket
            And I add the "Catch 22" to the basket
            Then the overall basket price should be EUR ??

    
    We have to find overall price basket for all scenarios based on the provided rules.
    We're assuming that all book prices include VAT. Is it clear from specs? It's worth to
    emphasize this part as well!

    
    After doing all calculations, we should have:
    
        Feature: Basket with different delivery cost
          In order to buy books
          As a customer
          I need to be able to put interesting books into a basket
        
          Rules:
          - VAT is 20% (included in all prices for a customer)
          - Delivery for basket under EUR 20 is EUR 3
          - Delivery for basket over EUR 20 is EUR 2
        
          Scenario: Buying a single book under EUR 20
            Given there is a "War and Peace", which costs EUR 17
            When I add the "War and Peace" to the basket
            Then the overall basket price should be EUR 20
        
          Scenario: Buying a single book over EUR 20
            Given there is a "Catch 22", which costs EUR 22
            When I add the "Catch 22" to the basket
            Then the overall basket price should be EUR 24
        
          Scenario: Buying two books over EUR 20
            Given there is a "War and Peace", which costs EUR 17
            And there is a "Catch 22", which costs EUR 22
            When I add the "War and Peace" to the basket
            And I add the "Catch 22" to the basket
            Then the overall basket price should be EUR 41

2. Let's move to write the implementation, the TDD way!

        class BasketTest extends TestCase
        {
            private $book1;
        
            private $book2;
        
            public function setUp()
            {
                $this->book1 = new Book("War and Peace", 17.0);
                $this->book2 = new Book("Catch 22", 22.0);
            }
        
            public function testTotalPriceForSingleBookUnder20Euros()
            {
                $basket = new Basket();
                $basket->addBook($this->book1);
                $this->assertEquals(17.0 + 3.0, $basket->getTotalPrice());
            }
        
            public function testTotalPriceForSingleBookOver20Euros()
            {
                $basket = new Basket();
                $basket->addBook($this->book2);
                $this->assertSame(22.0 + 2.0, $basket->getTotalPrice());
            }
        
            public function testTotalPriceForMultipleBooksOver20Euros()
            {
                $basket = new Basket();
                $basket->addBook($this->book1);
                $basket->addBook($this->book2);
                $this->assertSame(17.0 + 22.0 + 2.0, $basket->getTotalPrice());
            }
        }
    
    We have to implement `Basket`, `Book` with a few methods:
    
        class Basket
        {
            /** @var Book[] */
            private $books;
        
            public function addBook(Book $book)
            {
                $this->books[] = $book;
            }
        
            public function getTotalPrice(): float
            {
                $price = 0;
                foreach ($this->books as $book) {
                    $price += $book->price();
                }
                $price += $this->addDeliveryCost($price);
                return $price;
            }
        
            private function addDeliveryCost($price): float
            {
                if ($price < 20) {
                    return 3.0;
                } else {
                    return 2.0;
                }
            }
        }

        class Book
        {
            private $title;
        
            private $price;
        
            public function __construct(string $title, float $price)
            {
                $this->title = $title;
                $this->price = $price;
            }
        
            public function price(): float
            {
                return $this->price;
            }
        }


3. Writing tests and implementation first before writing BDD scenarios can help to implement BDD
tests the clean, well organized way. We can create reusable method, for example: to create different
books within the same methods.

        class FeatureContext implements Context
        {
            private $books;
        
            private $basket;
        
            /**
             * Initializes context.
             *
             * Every scenario gets its own context instance.
             * You can also pass arbitrary arguments to the
             * context constructor through behat.yml.
             */
            public function __construct()
            {
                $this->basket = new Basket;
            }
        
            /**
             * @Given there is a :title book, which costs EUR :price
             */
            public function thereIsAWhichCostsEur($title, $price)
            {
                $this->books[$title] = new Book($title, $price);
            }
        
            /**
             * @When I add the :title book to the basket
             */
            public function iAddTheToTheBasket($title)
            {
                if (!isset($this->books[$title])) {
                    throw new Exception('Book not defined');
                }
                $this->basket->addBook($this->books[$title]);
            }
        
            /**
             * @Then the overall basket price should be EUR :totalPrice
             */
            public function theOverallBasketPriceShouldBeEur($totalPrice)
            {
                Assert::assertEquals($totalPrice, $this->basket->getTotalPrice());
            }
        }

    
