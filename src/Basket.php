<?php

declare(strict_types=1);

namespace CODEfactors\BddByExample;

use CODEfactors\BddByExample\Specifications\FiveBooksSpecification;
use CODEfactors\BddByExample\Specifications\DeliveryCostSpecification;

class Basket
{
    /** @var Book[] */
    private $books;

    public function addBook(Book $book)
    {
        $this->books[] = $book;
    }

    public function getTotalPrice(): float
    {
        $price = 0;
        foreach ($this->books as $book) {
            $price += $book->price();
        }
        $fiveBooksSpec = new FiveBooksSpecification();
        if ($fiveBooksSpec->isSatisfiedBy($this->books)) {
            $price = $price - $this->getPriceReductionForFiveBooks();
        }
        $price += $this->addDeliveryCost($price);
        return $price;
    }

    private function addDeliveryCost($price): float
    {
        $deliveryCostSpec = new DeliveryCostSpecification();
        if ($deliveryCostSpec->isSatisfiedBy($price)) {
            return 2.0;
        } else {
            return 3.0;
        }
    }

    private function getPriceReductionForFiveBooks(): float
    {
        $cheapestPrice = 0;
        foreach ($this->books as $book) {
            if ($cheapestPrice === 0 || $book->price() < $cheapestPrice) {
                $cheapestPrice = $book->price();
            }
        }
        return $cheapestPrice;
    }
}
