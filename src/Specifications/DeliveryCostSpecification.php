<?php

declare(strict_types=1);

namespace CODEfactors\BddByExample\Specifications;

class DeliveryCostSpecification
{
    public function isSatisfiedBy(float $price): bool
    {
        return $price > 20;
    }
}
