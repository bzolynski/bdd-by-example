<?php

declare(strict_types=1);

namespace CODEfactors\BddByExample\Specifications;

class FiveBooksSpecification
{
    public function isSatisfiedBy(array $books): bool
    {
        return count($books) >= 5;
    }
}
