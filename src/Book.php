<?php

declare(strict_types=1);

namespace CODEfactors\BddByExample;

class Book
{
    private $title;

    private $price;

    public function __construct(string $title, float $price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    public function price(): float
    {
        return $this->price;
    }
}
