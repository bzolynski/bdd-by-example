<?php

declare(strict_types=1);

namespace CODEfactors\BddByExample\Tests;

use CODEfactors\BddByExample\Basket;
use CODEfactors\BddByExample\Book;
use PHPUnit\Framework\TestCase;

class BasketTest extends TestCase
{
    private $book1;

    private $book2;

    public function setUp()
    {
        $this->book1 = new Book("War and Peace", 17.0);
        $this->book2 = new Book("Catch 22", 22.0);
    }

    public function testTotalPriceForSingleBookUnder20Euros()
    {
        $basket = new Basket();
        $basket->addBook($this->book1);
        $this->assertEquals(17.0 + 3.0, $basket->getTotalPrice());
    }

    public function testTotalPriceForSingleBookOver20Euros()
    {
        $basket = new Basket();
        $basket->addBook($this->book2);
        $this->assertSame(22.0 + 2.0, $basket->getTotalPrice());
    }

    public function testTotalPriceForMultipleBooksOver20Euros()
    {
        $basket = new Basket();
        $basket->addBook($this->book1);
        $basket->addBook($this->book2);
        $this->assertSame(17.0 + 22.0 + 2.0, $basket->getTotalPrice());
    }

    public function testCheapestBook()
    {
        $basket = new Basket();
        for ($i = 1; $i <= 4; $i++) {
            $basket->addBook($this->book2);
        }
        $basket->addBook($this->book1);
        $this->assertSame(22.0 * 4 + 2.0, $basket->getTotalPrice());
    }

    public function testCheapestBookWhenAllBooksHaveTheSamePrice()
    {
        $basket = new Basket();
        for ($i = 1; $i <= 5; $i++) {
            $basket->addBook($this->book2);
        }
        $this->assertSame(22.0 * 4 + 2.0, $basket->getTotalPrice());
    }
}
